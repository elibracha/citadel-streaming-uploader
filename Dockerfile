FROM openjdk:13

WORKDIR /code
ARG APPLICATION_CREDENTIALS

COPY ${APPLICATION_CREDENTIALS} /secrets/scitadel-streaming-bd673c096407.json
ENV GOOGLE_APPLICATION_CREDENTIALS /secrets/scitadel-streaming-bd673c096407.json

COPY target/*SNAPSHOT.jar app.jar

CMD ["/usr/bin/java", "-jar", "-Dspring.profiles.active=dev", "app.jar"]