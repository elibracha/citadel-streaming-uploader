package org.citadel.steaming.exceptions;

public class StorageCredFilePathException extends RuntimeException{
	
	private static final long serialVersionUID = -3470389155464679546L;

	public StorageCredFilePathException(String message) {
		super(message);
	}
}
