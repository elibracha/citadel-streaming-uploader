package org.citadel.steaming.models;

import java.util.List;

public class ListBucketResponse {

	private List<BucketDetails> buckets;

	public ListBucketResponse() {
		super();
	}

	public ListBucketResponse(List<BucketDetails> buckets) {
		super();
		this.buckets = buckets;
	}

	public List<BucketDetails> getBuckets() {
		return buckets;
	}

	public void setBuckets(List<BucketDetails> buckets) {
		this.buckets = buckets;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buckets == null) ? 0 : buckets.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ListBucketResponse other = (ListBucketResponse) obj;
		if (buckets == null) {
			if (other.buckets != null)
				return false;
		} else if (!buckets.equals(other.buckets))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ListBucketResponse [buckets=" + buckets + "]";
	}
	
	
}
