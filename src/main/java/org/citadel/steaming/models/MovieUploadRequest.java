package org.citadel.steaming.models;

import java.sql.Date;

import org.springframework.web.multipart.MultipartFile;

public class MovieUploadRequest {
	
	private String bucketName;
	private MultipartFile file;
	private Date  date;
	
	public MovieUploadRequest() {
		super();
	}

	public MovieUploadRequest(String bucketName, MultipartFile file) {
		super();
		this.bucketName = bucketName;
		this.file = file;
		this.date = new Date(System.currentTimeMillis());
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bucketName == null) ? 0 : bucketName.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((file == null) ? 0 : file.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovieUploadRequest other = (MovieUploadRequest) obj;
		if (bucketName == null) {
			if (other.bucketName != null)
				return false;
		} else if (!bucketName.equals(other.bucketName))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (file == null) {
			if (other.file != null)
				return false;
		} else if (!file.equals(other.file))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MovieUploadRequest [bucketName=" + bucketName + ", file=" + file + ", date=" + date + "]";
	}
	
	
	
	
}
