package org.citadel.steaming.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BucketDetails {
	
	@JsonProperty("bucket_name")
	private String bucketName;
	@JsonProperty("bucket_create_time")
	private String bucketCreateTime;

	public BucketDetails() {
		super();
	}

	public BucketDetails(String bucketName, String bucketCreateTime) {
		super();
		this.bucketName = bucketName;
		this.bucketCreateTime = bucketCreateTime;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getBucketCreateTime() {
		return bucketCreateTime;
	}

	public void setBucketCreateTime(String bucketCreateTime) {
		this.bucketCreateTime = bucketCreateTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bucketCreateTime == null) ? 0 : bucketCreateTime.hashCode());
		result = prime * result + ((bucketName == null) ? 0 : bucketName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BucketDetails other = (BucketDetails) obj;
		if (bucketCreateTime == null) {
			if (other.bucketCreateTime != null)
				return false;
		} else if (!bucketCreateTime.equals(other.bucketCreateTime))
			return false;
		if (bucketName == null) {
			if (other.bucketName != null)
				return false;
		} else if (!bucketName.equals(other.bucketName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BucketDetails [bucketName=" + bucketName + ", bucketCreateTime=" + bucketCreateTime + "]";
	}
	 
}
