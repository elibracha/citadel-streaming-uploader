package org.citadel.steaming.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;

import javax.servlet.http.HttpServletResponse;

import org.citadel.steaming.models.ListBucketResponse;
import org.citadel.steaming.models.MovieUploadRequest;
import org.citadel.steaming.services.FileUploadBucketService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.api.client.util.IOUtils;
import com.google.cloud.ReadChannel;
import com.google.common.io.Resources;
import com.google.common.primitives.Longs;

@RestController
@RequestMapping("/api/v1")
public class UploadMovieController {

	private FileUploadBucketService fileUploadBucketService;

	public UploadMovieController(FileUploadBucketService fileUploadBucketService) {
		super();
		this.fileUploadBucketService = fileUploadBucketService;
	}

	@GetMapping(path = "/buckets", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ListBucketResponse getBuckets() {
		return fileUploadBucketService.getBuckets();
	}

	@PostMapping(path = "/bucket/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public void uploadToBucket(@RequestParam("file") MultipartFile file, @RequestParam("bucket") String bucketName)
			throws IOException {
		fileUploadBucketService.saveFile(new MovieUploadRequest(bucketName, file));
	}

	@GetMapping("/bucket/{name}")
	public void createBucket(@PathVariable("name") String bucketName) {
		fileUploadBucketService.createBucket(bucketName);
	}

	@GetMapping(value = "/bucket/stream")
    @ResponseBody
    public final ResponseEntity<InputStreamResource> retrieveResource(@RequestHeader(value = "Range", required = false) String range) throws Exception {

    long rangeStart = Longs.tryParse(range.replace("bytes=","").split("-")[0]);
    long rangeEnd = Longs.tryParse(range.replace("bytes=","").split("-")[1]);
    long contentLenght = 31491130; //you must have it somewhere stored or read the full file size

    InputStream inputStream = Resources.getResource("video.mp4").openStream();//or read from wherever your data is into stream
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.asMediaType(MimeType.valueOf("video/mp4")));
    headers.set("Accept-Ranges", "bytes");
    headers.set("Expires", "0");
    headers.set("Cache-Control", "no-cache, no-store");
    headers.set("Connection", "keep-alive");
    headers.set("Content-Transfer-Encoding", "binary");
    headers.set("Content-Length", String.valueOf(rangeEnd - rangeStart));

    if (rangeStart == 0) {
      return new ResponseEntity<InputStreamResource>(new InputStreamResource(inputStream), headers, HttpStatus.OK);
    } else {
      headers.set("Content-Range", String.format("bytes %s-%s/%s", rangeStart, rangeEnd, contentLenght));
	return new ResponseEntity<InputStreamResource>(new InputStreamResource(inputStream), headers, HttpStatus.PARTIAL_CONTENT);
    }
  }
}
