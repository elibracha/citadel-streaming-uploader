package org.citadel.steaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BucketUploadApplication {

	public static void main(String[] args) {
		SpringApplication.run(BucketUploadApplication.class, args);
	}

}
