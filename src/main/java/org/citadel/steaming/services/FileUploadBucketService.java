package org.citadel.steaming.services;

import java.io.IOException;
import java.util.Date;
import java.util.stream.Collectors;

import org.citadel.steaming.models.BucketDetails;
import org.citadel.steaming.models.ListBucketResponse;
import org.citadel.steaming.models.MovieUploadRequest;
import org.springframework.stereotype.Service;

import com.google.cloud.ReadChannel;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.Storage;
import com.google.common.collect.Lists;


@Service
public class FileUploadBucketService {

	private Storage storage;
	
	public FileUploadBucketService(Storage storage) {
		this.storage = storage;
	}
	
	public ListBucketResponse getBuckets() {
		return new ListBucketResponse(Lists.newArrayList(storage.list().iterateAll())
						.stream()
						.map(bucket -> 
									new BucketDetails(bucket.getName(),
											new Date(bucket.getCreateTime()).toString()))
						.collect(Collectors.toList())
						);
	}
	
	public void createBucket(String bucketName) {
		storage.create(BucketInfo.of(bucketName));
	}
	
	public void saveFile(MovieUploadRequest request) throws IOException {
		boolean bucketExist = false;
		
		BlobId blobId = BlobId.of(request.getBucketName(), request.getFile().getName());
		BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("video/mp4").build();
		
		for (Bucket bucket : storage.list().iterateAll()) {
			if (bucket.getName().equals(request.getBucketName()))
				bucketExist = true;
		}
		
		if (!bucketExist) { 
			storage.create(BucketInfo.of(request.getBucketName()));
		}
		
		storage.create(blobInfo, request.getFile().getInputStream().readAllBytes());
	}
	
	public ReadChannel getVideo(String videoName) throws IOException {
		String bucketName = "citadel-streaming-movies";
		String blobName = videoName;
		 
		return  storage.reader(bucketName, blobName);
	}
}
