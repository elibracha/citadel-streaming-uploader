package org.citadel.steaming.config;

import java.io.FileInputStream;
import java.io.IOException;

import org.citadel.steaming.exceptions.StorageCredFilePathException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.google.api.gax.retrying.RetrySettings;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

@Configuration
public class StorageCloudConfig {

	@Value("${GOOGLE_APPLICATION_CREDENTIALS}")
	private String secret;

	@Bean
	@Primary
	public Storage storage() {
		try {
			return StorageOptions.newBuilder()
				    .setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream(secret)))
				    .setRetrySettings(RetrySettings.newBuilder().setMaxAttempts(5).build())
				    .build()
				    .getService();
		} catch (IOException e) {
			throw new StorageCredFilePathException("credentials file path is invalid or could not be accessed.");
		}
	}
}
